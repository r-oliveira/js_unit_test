# Karma/Jasmine testing suite project

Esse projeto tem como objetivo viabilizar o uso de testes unitários e definir padrões de desenvolvimento no front-end dos apps.

## Sobre

Atualmente, o projeto conta com uma estrutura baseada em MVVM e utiliza AngularJS.


## Instruções

- Para iniciar, clone este repositório e digite o comando *npm install* no terminal, com isso você terá todas as dependências necessárias para trabalhar.
- Digitar o comando *gulp serve* para iniciar o projeto

## Testes 

- Digite o comando *npm test* para iniciar o karma e acompanhar os testes