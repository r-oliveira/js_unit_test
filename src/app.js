(function () {
    'use strict';

    var app = angular.module('testApp', [
    ]);

    app.controller('testCtrl', testCtrl);

    function testCtrl($scope){
        $scope.testValue = 2;
    }

})();
