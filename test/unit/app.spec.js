
describe('Start app testing suite', function () {
    beforeEach(module('testApp'));

    var $scope = {};
    var $controller;

    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
    }));

    describe('Unit: testing testCtrl', function () {
        it('tests $scope.testValue', function () {
            var controller = $controller('testCtrl', { $scope: $scope });
            expect($scope.testValue).toBeDefined();
            expect($scope.testValue).toBeGreaterThan(1);
            expect($scope.testValue).toBeLessThanOrEqual(2);
        });
      
    });

});
