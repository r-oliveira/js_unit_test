'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const inject = require('gulp-inject');

let js = [
  'node_modules/angular/angular.js',
  'node_modules/angular-mocks/angular-mocks.js',
  'src/app.js'
];

gulp.task('serve', function () {
  browserSync.init({
    server: {
       baseDir: ["./", "./src" ]
    }
  });
  gulp.watch("./src/index.html").on("change", reload);
});

// gulp.task('index', function () {
//   var target = gulp.src('./src/index.html');
//   return target.pipe(inject(gulp.src(js, { read: false }), { relative: true }))
//     .pipe(gulp.dest('./src'));
// });
